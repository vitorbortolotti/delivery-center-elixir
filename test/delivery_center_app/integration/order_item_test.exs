defmodule DeliveryCenterApp.Integration.OrderItemTest do
  use DeliveryCenterApp.DataCase, async: true

  alias DeliveryCenterApp.Integration.OrderItem

  describe "OrderItem.changeset/2" do
    @invalid_attrs %{
      external_code: nil,
      name: nil,
      price: nil,
      quantity: nil,
      total: nil,
      sub_items: nil,
    }

    @valid_attrs %{
      external_code: "IT4801901403",
      name: "Produto de Testes",
      price: 49.9,
      quantity: 1,
      total: 49.9,
      sub_items: []
    }

    test "changeset with invalid attributes" do
      changeset = OrderItem.changeset(%OrderItem{}, @invalid_attrs)

      refute changeset.valid?
    end

    test "changeset with valid attributes" do
      changeset = OrderItem.changeset(%OrderItem{}, @valid_attrs)

      assert changeset.valid?
    end
  end

end
