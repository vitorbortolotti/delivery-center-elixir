defmodule DeliveryCenterApp.Integration.OrderBuilderTest do
  use DeliveryCenterApp.DataCase, async: true

  alias DeliveryCenterApp.Integration.OrderBuilder

  describe "Order.changeset/2" do
    @invalid_attrs %{}

    @valid_attrs %{
      "id" => 9987071,
      "store_id" => 282,
      "date_created" => "2019-06-24T16:45:32.000-04:00",
      "date_closed" => "2019-06-24T16:45:35.000-04:00",
      "last_updated" => "2019-06-25T13:26:49.000-04:00",
      "total_amount" => 49.9,
      "total_amount_with_shipping" => 55.04,
      "paid_amount" => 55.04,
      "expiration_date" => "2019-07-22T16:45:35.000-04:00",
      "total_shipping" => 5.14,
      "order_items" => [
        %{
          "item" => %{
            "id" => "IT4801901403",
            "title" => "Produto de Testes"
          },
          "quantity" => 1,
          "unit_price" => 49.9,
          "full_unit_price" => 49.9
        }
      ],
      "payments" => [
        %{
          "id" => 12312313,
          "order_id" => 9987071,
          "payer_id" => 414138,
          "installments" => 1,
          "payment_type" => "credit_card",
          "status" => "paid",
          "transaction_amount" => 49.9,
          "taxes_amount" => 0,
          "shipping_cost" => 5.14,
          "total_paid_amount" => 55.04,
          "installment_amount" => 55.04,
          "date_approved" => "2019-06-24T16:45:35.000-04:00",
          "date_created" => "2019-06-24T16:45:33.000-04:00"
        }
      ],
      "shipping" => %{
        "id" => 43444211797,
        "shipment_type" => "shipping",
        "date_created" => "2019-06-24T16:45:33.000-04:00",
        "receiver_address" => %{
          "id" => 1051695306,
          "address_line" => "Rua Fake de Testes 3454",
          "street_name" => "Rua Fake de Testes",
          "street_number" => "3454",
          "comment" => "teste",
          "zip_code" => "85045020",
          "city" => %{
            "name" => "Cidade de Testes"
          },
          "state" => %{
            "name" => "São Paulo"
          },
          "country" => %{
            "id" => "BR",
            "name" => "Brasil"
          },
          "neighborhood" => %{
            "id" => nil,
            "name" => "Vila de Testes"
          },
          "latitude" => -23.629037,
          "longitude" => -46.712689,
          "receiver_phone" => "41999999999"
        }
      },
      "status" => "paid",
      "buyer" => %{
        "id" => 136226073,
        "nickname" => "JOHN DOE",
        "email" => "john@doe.com",
        "phone" => %{
          "area_code" => 41,
          "number" => "999999999"
        },
        "first_name" => "John",
        "last_name" => "Doe",
        "billing_info" => %{
          "doc_type" => "CPF",
          "doc_number" => "09487965477"
        }
      }
    }

    test "build base with valid attrs" do
      attrs = OrderBuilder.build(@valid_attrs)
      assert attrs.external_code == "9987071"
      assert attrs.store_id == 282
      assert attrs.sub_total == "49.90"
      assert attrs.delivery_fee == "5.14"
      assert attrs.total_shipping == "5.14"
      assert attrs.total == "55.04"
      assert attrs.country == "BR"
      assert attrs.state == "SP"
      assert attrs.city == "Cidade de Testes"
      assert attrs.district == "Vila de Testes"
      assert attrs.street == "Rua Fake de Testes"
      assert attrs.complement == "teste"
      assert attrs.latitude == -23.629037
      assert attrs.longitude ==  -46.712689
      assert attrs.dt_order_create == "2019-06-24T20:45:32.000Z"
      assert attrs.postal_code == "85045020"
      assert attrs.number == "3454"
    end

    test "build customer with valid attrs" do
      attrs = OrderBuilder.build(@valid_attrs)
      assert attrs.customer.external_code == "136226073"
      assert attrs.customer.name == "JOHN DOE"
      assert attrs.customer.email == "john@doe.com"
      assert attrs.customer.contact == "41999999999"
    end

    test "build payments with valid attrs" do
      attrs = OrderBuilder.build(@valid_attrs)
      assert List.first(attrs.payments).type == "CREDIT_CARD"
      assert List.first(attrs.payments).value == 55.04
    end

    test "build items with valid attrs" do
      attrs = OrderBuilder.build(@valid_attrs)
      assert List.first(attrs.items).external_code == "IT4801901403"
      assert List.first(attrs.items).name == "Produto de Testes"
      assert List.first(attrs.items).price == 49.9
      assert List.first(attrs.items).quantity == 1
      assert List.first(attrs.items).total == 49.9
      assert List.first(attrs.items).sub_items == []
    end
  end

  describe "transform_to_string/1" do

    test "when input is nil it returns nil" do
      assert OrderBuilder.transform_to_string(nil) == nil
    end

    test "when input is not nil it returns a stringified version of the number" do
      assert OrderBuilder.transform_to_string(1231434) == "1231434"
    end

  end

  describe "transform_price" do

    test "when input is nil it returns nil" do
      assert OrderBuilder.transform_price(nil) == nil
    end

    test "when input is not nil it returns the price formatted as string" do
      assert OrderBuilder.transform_price(40.0) == "40.00"
      assert OrderBuilder.transform_price(3.0) == "3.00"
      assert OrderBuilder.transform_price(0.1) == "0.10"
      assert OrderBuilder.transform_price(23.456) == "23.46"
      assert OrderBuilder.transform_price(0.125) == "0.13"
      assert OrderBuilder.transform_price(43000.0) == "43000.00"
    end

  end

  describe "transform_state" do

    test "when input is nil it returns nil" do
      assert OrderBuilder.transform_state(nil) == nil
    end

    test "when input is not known state it returns nil" do
      assert OrderBuilder.transform_state("Lorem") == nil
    end

    test "when input is a known state it returns the state abbreviation" do
      assert OrderBuilder.transform_state("Espírito Santo") == "ES"
    end

  end

end
