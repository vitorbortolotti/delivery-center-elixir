defmodule DeliveryCenterApp.Integration.PaymentTest do
  use DeliveryCenterApp.DataCase, async: true

  alias DeliveryCenterApp.Integration.Payment

  describe "Payment.changeset/2" do
    @invalid_attrs %{
      type: nil,
      value: nil,
    }

    @valid_attrs %{
      type: "CREDIT_CARD",
      value: 55.04,
    }

    test "changeset with invalid attributes" do
      changeset = Payment.changeset(%Payment{}, @invalid_attrs)

      refute changeset.valid?
    end

    test "changeset with valid attributes" do
      changeset = Payment.changeset(%Payment{}, @valid_attrs)

      assert changeset.valid?
    end
  end

end
