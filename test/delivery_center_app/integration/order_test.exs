defmodule DeliveryCenterApp.Integration.OrderTest do
  use DeliveryCenterApp.DataCase, async: true

  alias DeliveryCenterApp.Integration.Order

  describe "Order.changeset/2" do
    @invalid_attrs %{}

    @valid_attrs %{
      external_code: "9987071",
      store_id: 282,
      sub_total: "49.90",
      delivery_fee: "5.14",
      total_shipping: "5.14",
      total: "55.04",
      country: "BR",
      state: "SP",
      city: "Cidade de Testes",
      district: "Vila de Testes",
      street: "Rua Fake de Testes",
      complement: "teste",
      latitude: -23.629037,
      longitude:  -46.712689,
      dt_order_create: "2019-06-24T16:45:32.000-04:00",
      postal_code: "85045020",
      number: "3454",
      customer: %{
        external_code: "136226073",
        name: "JOHN DOE",
        email: "john@doe.com",
        contact: "41999999999",
      },
      items: [
        %{
            external_code: "IT4801901403",
            name: "Produto de Testes",
            price: 49.9,
            quantity: 1,
            total: 49.9,
            sub_items: []
        }
      ],
      payments: [
        %{
            type: "CREDIT_CARD",
            value: 55.04
        }
      ]
    }

    test "changeset with invalid attributes" do
      changeset = Order.changeset(%Order{}, @invalid_attrs)
      refute changeset.valid?
    end

    test "changeset with valid attributes" do
      changeset = Order.changeset(%Order{}, @valid_attrs)
      assert changeset.valid?
    end
  end

end
