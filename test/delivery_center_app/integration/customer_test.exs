defmodule DeliveryCenterApp.Integration.CustomerTest do
  use DeliveryCenterApp.DataCase, async: true

  alias DeliveryCenterApp.Integration.Customer

  describe "Customer.changeset/2" do
    @invalid_attrs %{
      external_code: nil,
      name: nil,
      email: nil,
      contact: nil,
    }

    @valid_attrs %{
      external_code: "136226073",
      name: "JOHN DOE",
      email: "john@doe.com",
      contact: "41999999999",
    }

    test "changeset with invalid attributes" do
      changeset = Customer.changeset(%Customer{}, @invalid_attrs)

      refute changeset.valid?
    end

    test "changeset with valid attributes" do
      changeset = Customer.changeset(%Customer{}, @valid_attrs)

      assert changeset.valid?
    end
  end

end
