# Delivery Center Elixir Interview

This project is the result of the Elixir interview test proposed by Delivery Center. 
The test requirements can be found [here](https://bitbucket.org/delivery_center/test-dev-backend2/src/master/).

## The solution

Being this my first time working with Elixir (or any functional language for that matter), my main objective was to develop a solution that respects the best practices of FP and Phoenix, with a simple and reliable integration API.

The solution is very similar to the approach used in the [Ruby Solution](https://bitbucket.org/vitorbortolotti/delivery-center-ruby/src/master/), inspired by the 
[Builder Design Pattern](https://sourcemaking.com/design_patterns/builder). Here the module `OrderBuilder` is 
responsible for building a map of attributes to create an `Order`, using the JSON payload sent by the marketplace. Once an `Order` is created and validated, a request is made to the Delivery Center API to complete the integration. 

The stack is a Phoenix 1.4, with:
 
- [Poison](https://github.com/devinus/poison) for object serialization
- [HTTPoison](https://github.com/edgurgel/httpoison) for HTTP/REST requests
- [Timex](https://github.com/bitwalker/timex) for Date/Time manipulation

### Relevant keypoints

- None of the models of the app are persisted in the database. Trying to normalize and persist all data passing through the application usually adds unnecessary complexity and makes it harder to mantain. However, it would be nice to have a logging mechanism to increase visibility of the requests processed in the app.
- The serialization of the maps in being done manually using the serialize function to make possible having all keys serialized in camelCase, except for :total_shipping, that uses snake_case. Though I imagine there's a more sophisticated way to do it.

## Getting Started

> If you don't feel like running the app locally, you can test it using the Heroku version: https://hidden-hamlet-66417.herokuapp.com

To test the app locally, make sure you have Docker and Docker Compose installed, then run 

```
docker-compose build
docker-compose run phoenix mix ecto.create
docker-compose up
```

If everything goes right, the app should be serving at http://localhost:4000.

## Usage

The API has only two endpoints for testing the integration flow. Both expect a POST with the body being 
a JSON structured as provided the test requirements.

### `POST /api/order_create`

Builds the processed json (if the validation passes) and makes a request to the Delivery Center API 
(https://delivery-center-recruitment-ap.herokuapp.com) with the constructed payload.

Example response:

```json
{
  "api_response": "OK",
  "success": true
}
```

[Here](https://gist.github.com/vitorbortolotti/ca7b74d176fafc051c190e9a48b8a63b) you can find a gist with 
an example curl request

### `POST /api/order_parse`

Builds the processed JSON (if the validation passes) and returns the serialized object. 

> The JSON returned here is exactly the payload that will be sent to the Delivery Center API in the 
> `/order_create` endpoint.

Example response:

```json
{
  "city": "Cidade de Testes",
  "complement": "teste",
  "country": "BR",
  ...
}
```

[Here](https://gist.github.com/vitorbortolotti/057098b80cc78c3129c6b13eb4715444) you can find a gist with 
an example curl request

