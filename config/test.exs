use Mix.Config

# Configure your database
config :delivery_center_app, DeliveryCenterApp.Repo,
  username: "postgres",
  password: "postgres",
  database: "delivery_center_app_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :delivery_center_app, DeliveryCenterAppWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn
