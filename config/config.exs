# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :delivery_center_app,
  ecto_repos: [DeliveryCenterApp.Repo],
  generators: [binary_id: true]

# Configures the endpoint
config :delivery_center_app, DeliveryCenterAppWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "8bEYVMk3mi6r9/k3mVHeYU/kauUqmsTYrTMdQc+LOWg/8BCDHd4gjpsUc37uKwsi",
  render_errors: [view: DeliveryCenterAppWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: DeliveryCenterApp.PubSub, adapter: Phoenix.PubSub.PG2],
  live_view: [signing_salt: "LUjv3awL"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
