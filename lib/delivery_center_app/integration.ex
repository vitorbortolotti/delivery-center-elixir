defmodule DeliveryCenterApp.Integration do
  @moduledoc """
  The Integration context.
  """

  import Ecto.Query, warn: false

  alias DeliveryCenterApp.Integration.Order
  alias DeliveryCenterApp.Integration.OrderBuilder

  @doc """
  Creates a order.

  ## Examples

      iex> create_order(%{field: value})
      {:ok, %Order{}}

      iex> create_order(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_order(params \\ %{}) do
    attrs = OrderBuilder.build(params)
    %Order{}
      |> Order.changeset(attrs)
      |> Ecto.Changeset.apply_action(:insert)
  end
end
