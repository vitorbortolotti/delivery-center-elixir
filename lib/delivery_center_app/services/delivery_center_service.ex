defmodule DeliveryCenterApp.Services.DeliveryCenterService do

  alias DeliveryCenterApp.Integration.Order

  @api_url "https://delivery-center-recruitment-ap.herokuapp.com/"

  def create_order(%Order{} = order) do
    body = order
      |> Order.serialize
      |> Poison.encode!
    headers = [
      {"Content-type", "application/json"},
      {"X-Sent", Timex.local()|> Timex.format!("%Hh%M - %d/%m/%Y", :strftime)}
    ]
    HTTPoison.post(@api_url, body, headers, [])
  end

end
