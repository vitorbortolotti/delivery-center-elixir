defmodule DeliveryCenterApp.Integration.Customer do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Jason.Encoder, except: [:id]}

  embedded_schema do
    field(:external_code, :string)
    field(:name, :string)
    field(:email, :string)
    field(:contact, :string)
  end

  @required_fields [:external_code, :name, :email, :contact]

  @doc false
  def changeset(customer, attrs) do
    customer
    |> cast(attrs, @required_fields)
    |> validate_required(@required_fields)
  end

  def serialize(customer) do
    %{
      externalCode: customer.external_code,
      name: customer.name,
      email: customer.email,
      contact: customer.contact,
    }
  end
end
