defmodule DeliveryCenterApp.Integration.Payment do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Jason.Encoder, except: [:id]}

  embedded_schema do
    field(:type, :string)
    field(:value, :float)
  end

  @required_fields [:type, :value]

  @doc false
  def changeset(payment, attrs) do
    payment
    |> cast(attrs, @required_fields)
    |> validate_required(@required_fields)
  end

  def serialize(payment), do: payment
end
