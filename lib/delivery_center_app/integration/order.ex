defmodule DeliveryCenterApp.Integration.Order do
  use Ecto.Schema
  import Ecto.Changeset

  alias DeliveryCenterApp.Integration.Customer
  alias DeliveryCenterApp.Integration.Payment
  alias DeliveryCenterApp.Integration.Customer
  alias DeliveryCenterApp.Integration.OrderItem

  @derive {Jason.Encoder, except: [:id]}

  embedded_schema do
    field(:external_code, :string)
    field(:store_id, :integer)
    field(:sub_total, :string)
    field(:delivery_fee, :string)
    field(:total_shipping, :string)
    field(:total, :string)
    field(:dt_order_create, :string)
    field(:country, :string)
    field(:city, :string)
    field(:district, :string)
    field(:street, :string)
    field(:number, :string)
    field(:complement, :string)
    field(:latitude, :float)
    field(:longitude, :float)
    field(:postal_code, :string)
    field(:state, :string)
    embeds_one :customer, Customer
    embeds_many :payments, Payment
    embeds_many :items, OrderItem
  end

  @required_fields [:external_code, :store_id, :dt_order_create, :sub_total,
  :delivery_fee, :total_shipping, :total, :country, :city, :district, :street, :number, :complement,
  :latitude, :longitude, :postal_code, :state]

  @doc false
  def changeset(order, attrs) do
    order
    |> cast(attrs, @required_fields)
    |> cast_embed(:customer)
    |> cast_embed(:payments)
    |> cast_embed(:items)
    |> validate_required(@required_fields)
  end

  def serialize(order) do
    %{
      externalCode: order.external_code,
      storeId: order.store_id,
      subTotal: order.sub_total,
      deliveryFee: order.delivery_fee,
      total_shipping: order.total_shipping,
      total: order.total,
      country: order.country,
      state: order.state,
      city: order.city,
      district: order.district,
      street: order.street,
      complement: order.complement,
      latitude: order.latitude,
      longitude: order.longitude,
      dtOrderCreate: order.dt_order_create,
      postalCode: order.postal_code,
      number: order.number,
      customer: order.customer |> Customer.serialize,
      items: Enum.map(order.items, fn i -> i |> OrderItem.serialize end),
      payments: Enum.map(order.payments, fn p -> p |> Payment.serialize end)
    }
  end
end
