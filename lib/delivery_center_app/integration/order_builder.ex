defmodule DeliveryCenterApp.Integration.OrderBuilder do

  alias DeliveryCenterApp.Helpers.BrazilStates

  def build(params) do
    %{
      store_id:         get_in(params, ["store_id"]),
      external_code:    get_in(params, ["id"]) |> transform_to_string,
      sub_total:        get_in(params, ["total_amount"]) |> transform_price,
      delivery_fee:     get_in(params, ["total_shipping"]) |> transform_price,
      total_shipping:   get_in(params, ["total_shipping"]) |> transform_price,
      total:            get_in(params, ["total_amount_with_shipping"]) |> transform_price,
      dt_order_create:  get_in(params, ["date_created"]) |> transform_date,
      country:          get_in(params, ["shipping", "receiver_address", "country", "id"]),
      city:             get_in(params, ["shipping", "receiver_address", "city", "name"]),
      district:         get_in(params, ["shipping", "receiver_address", "neighborhood", "name"]),
      street:           get_in(params, ["shipping", "receiver_address", "street_name"]),
      number:           get_in(params, ["shipping", "receiver_address", "street_number"]),
      complement:       get_in(params, ["shipping", "receiver_address", "comment"]),
      latitude:         get_in(params, ["shipping", "receiver_address", "latitude"]),
      longitude:        get_in(params, ["shipping", "receiver_address", "longitude"]),
      postal_code:      get_in(params, ["shipping", "receiver_address", "zip_code"]),
      state:            get_in(params, ["shipping", "receiver_address", "state", "name"]) |> transform_state,
      customer:         build_customer(params),
      payments:         build_payments(params),
      items:            build_items(params),
    }
  end

  def build_customer(params) do
    %{
      external_code: Integer.to_string(get_in(params, ["buyer", "id"])),
      name:          get_in(params, ["buyer", "nickname"]),
      email:         get_in(params, ["buyer", "email"]),
      contact:       "#{get_in(params, ["buyer", "phone", "area_code"])}#{get_in(params, ["buyer", "phone", "number"])}",
    }
  end

  def build_payments(params) do
    Enum.map(params["payments"], fn payment ->
      %{
        type: payment["payment_type"] |> transform_payment_type,
        value: payment["total_paid_amount"]
      }
    end)
  end

  def build_items(params) do
    Enum.map(params["order_items"], fn item ->
      %{
        external_code: get_in(item, ["item", "id"]),
        name:          get_in(item, ["item", "title"]),
        price:         get_in(item, ["unit_price"]),
        quantity:      get_in(item, ["quantity"]),
        total:         get_in(item, ["full_unit_price"]),
        sub_items:     [],
      }
    end)
  end

  def transform_to_string(nil), do: nil
  def transform_to_string(integer) do
    Integer.to_string(integer)
  end

  def transform_price(nil), do: nil
  def transform_price(price) do
    :erlang.float_to_binary(price, [decimals: 2])
  end

  def transform_state(nil), do: nil
  def transform_state(state) do
    BrazilStates.abbreviate(state)
  end

  def transform_payment_type(nil), do: nil
  def transform_payment_type(payment_type) do
    # Assuming there would exist more than one payment type
    case payment_type do
      "credit_card" -> "CREDIT_CARD"
      _ -> nil
    end
  end

  def transform_date(nil), do: nil
  def transform_date(date) do
    case DateTime.from_iso8601(date) do
      {:ok, datetime, _} -> DateTime.to_iso8601(datetime)
      {:error, reason} -> raise "Invalid date: #{reason}"
    end
  end

end
