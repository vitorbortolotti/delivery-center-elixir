defmodule DeliveryCenterApp.Integration.OrderItem do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Jason.Encoder, except: [:id]}

  embedded_schema do
    field(:external_code, :string)
    field(:name, :string)
    field(:price, :float)
    field(:quantity, :integer)
    field(:total, :float)
    field(:sub_items, {:array, :string})
  end

  @required_fields [:external_code, :name, :price, :quantity, :total, :sub_items]

  @doc false
  def changeset(order_item, attrs) do
    order_item
    |> cast(attrs, @required_fields)
    |> validate_required(@required_fields)
  end

  def serialize(order_item) do
    %{
      externalCode: order_item.external_code,
      name: order_item.name,
      price: order_item.price,
      quantity: order_item.quantity,
      total: order_item.total,
      subItems: order_item.sub_items,
    }
  end
end
