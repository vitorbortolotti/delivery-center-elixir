defmodule DeliveryCenterApp.Helpers.BrazilStates do

  def find_by(key, value) do
    Enum.find(get_states(), fn map -> map[key] == value end)
  end

  def abbreviate(value) do
    case find_by(:name, value) do
      nil -> nil
      map -> Map.get(map, :abbreviation)
    end
  end

  def get_states do
    [
      %{
        abbreviation: "AC",
        name: "Acre",
      },
      %{
          abbreviation: "AL",
          name: "Alagoas",
      },
      %{
          abbreviation: "AP",
          name: "Amapá",
      },
      %{
          abbreviation: "AM",
          name: "Amazonas",
      },
      %{
          abbreviation: "BA",
          name: "Bahia",
      },
      %{
          abbreviation: "CE",
          name: "Ceará",
      },
      %{
          abbreviation: "DF",
          name: "Distrito Federal",
      },
      %{
          abbreviation: "ES",
          name: "Espírito Santo",
      },
      %{
          abbreviation: "GO",
          name: "Goiás",
      },
      %{
          abbreviation: "MA",
          name: "Maranhão",
      },
      %{
          abbreviation: "MT",
          name: "Mato Grosso",
      },
      %{
          abbreviation: "MS",
          name: "Mato Grosso do Sul",
      },
      %{
          abbreviation: "MG",
          name: "Minas Gerais",
      },
      %{
          abbreviation: "PA",
          name: "Pará",
      },
      %{
          abbreviation: "PB",
          name: "Paraíba",
      },
      %{
          abbreviation: "PR",
          name: "Paraná",
      },
      %{
          abbreviation: "PE",
          name: "Pernambuco",
      },
      %{
          abbreviation: "PI",
          name: "Piauí",
      },
      %{
          abbreviation: "RJ",
          name: "Rio de Janeiro",
      },
      %{
          abbreviation: "RN",
          name: "Rio Grande do Norte",
      },
      %{
          abbreviation: "RS",
          name: "Rio Grande do Sul",
      },
      %{
          abbreviation: "RO",
          name: "Rondônia",
      },
      %{
          abbreviation: "RR",
          name: "Roraima",
      },
      %{
          abbreviation: "SC",
          name: "Santa Catarina",
      },
      %{
          abbreviation: "SP",
          name: "São Paulo",
      },
      %{
          abbreviation: "SE",
          name: "Sergipe",
      },
      %{
          abbreviation: "TO",
          name: "Tocantins",
      }
    ]
  end

end
