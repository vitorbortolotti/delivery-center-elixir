defmodule DeliveryCenterApp.Repo do
  use Ecto.Repo,
    otp_app: :delivery_center_app,
    adapter: Ecto.Adapters.Postgres
end
