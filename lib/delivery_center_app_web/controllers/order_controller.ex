defmodule DeliveryCenterAppWeb.OrderController do
  use DeliveryCenterAppWeb, :controller

  alias DeliveryCenterAppWeb.ErrorView
  alias DeliveryCenterApp.Integration
  alias DeliveryCenterApp.Services.DeliveryCenterService

  action_fallback DeliveryCenterAppWeb.FallbackController

  def create(conn, order_params) do
    case Integration.create_order(order_params) do
      {:ok, order} ->
        case DeliveryCenterService.create_order(order) do
          {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
            conn
              |> put_status(:created)
              |> render("success.json", body: body)

          {:error, %HTTPoison.Error{reason: reason}} ->
            conn
              |> put_status(:internal_error)
              |> render(ErrorView, "500.json", reason: reason)
        end

      {:error, changeset} ->
        conn
          |> put_status(:bad_request)
          |> render(ErrorView, "400.json", changeset: changeset)
    end
  end

  def parse(conn, order_params) do
    case Integration.create_order(order_params) do
      {:ok, order} ->
        conn
          |> put_status(:ok)
          |> render("order.json", order: order)

      {:error, changeset} ->
        conn
          |> put_status(:bad_request)
          |> render(ErrorView, "400.json", changeset: changeset)
    end
  end
end
