defmodule DeliveryCenterAppWeb.OrderView do
  use DeliveryCenterAppWeb, :view
  alias DeliveryCenterAppWeb.OrderView
  alias DeliveryCenterApp.Integration.Order

  def render("index.json", %{orders: orders}) do
    %{data: render_many(orders, OrderView, "order.json")}
  end

  def render("show.json", %{order: order}) do
    %{data: render_one(order, OrderView, "order.json")}
  end

  def render("order.json", %{order: order}) do
    order |> Order.serialize
  end

  def render("success.json", %{body: body}) do
    %{success: true, api_response: body}
  end
end
