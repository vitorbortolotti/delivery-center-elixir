defmodule DeliveryCenterAppWeb.ErrorView do
  use DeliveryCenterAppWeb, :view

  # If you want to customize a particular status code
  # for a certain format, you may uncomment below.
  # def render("500.json", _assigns) do
  #   %{errors: %{detail: "Internal Server Error"}}
  # end

  def render("400.json", %{body: body}) do
    %{success: false, body: body}
  end
  def render("400.json", %{changeset: changeset}) do
    %{success: false, changeset: Ecto.Changeset.traverse_errors(changeset, &translate_error/1)}
  end

  def render("422.json", %{reason: reason}) do
    %{success: false, reason: reason}
  end

  # By default, Phoenix returns the status message from
  # the template name. For example, "404.json" becomes
  # "Not Found".
  def template_not_found(template, _assigns) do
    %{errors: %{detail: Phoenix.Controller.status_message_from_template(template)}}
  end
end
