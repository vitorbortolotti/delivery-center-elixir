defmodule DeliveryCenterAppWeb.Router do
  use DeliveryCenterAppWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", DeliveryCenterAppWeb do
    pipe_through :api
    post "/order_create", OrderController, :create
    post "/order_parse", OrderController, :parse
  end
end
